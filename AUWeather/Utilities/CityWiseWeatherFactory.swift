//
//  CityWiseWeatherFactory.swift
//  AUWeather
//
//  Created on 01/11/17.
//

import Foundation

class CityWiseWeatherFactory {
    
    var delegate: CityWiseWeatherFactoryProtocol?
    let cityIds = ["4163971", "2147714", "2174003"]
    var cityDictionary = Dictionary<String, Any>()
    
    static let sharedFactory = CityWiseWeatherFactory()
    
    func configure(withDelegate delegate: CityWiseWeatherFactoryProtocol){
        self.delegate = delegate
    }
    
    func getWeatherDetails() {
        
        for cityId in cityIds {
            AUWeatherAPIClient.sharedClient.getWeatherDetailsForCity(withId: cityId, completionHandler: { [weak self] (weatherDictionary, error) in
                if error == nil {
                    let id = weatherDictionary!["id"] as! NSNumber
                    self?.delegate?.newWeatherDataRecorded(cityId: id.stringValue, weatherDetails: weatherDictionary!)
                }else {
                    self?.delegate?.errorInGettingDetails((error!.localizedDescription))
                }
                
            })
        }
    }
    
}

protocol CityWiseWeatherFactoryProtocol {
    func newWeatherDataRecorded(cityId: String, weatherDetails: Dictionary<String, Any>)
    func errorInGettingDetails(_ error: String)
}
