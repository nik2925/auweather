//
//  DictionaryOperations.swift
//  AUWeather
//
//  Created on 02/11/17.
//

import Foundation

class DictionaryOperations {
    
    var dictionaryToOperate = Dictionary<String, Any>()
    
    init(withDictionary dictionary: Dictionary<String, Any>) {
        self.dictionaryToOperate = dictionary
    }
    
    func getCity() -> String {
        return dictionaryToOperate["name"] as! String
    }
    
    func getTemperature() -> String {
        let mainDict = dictionaryToOperate["main"] as! Dictionary<String, Any>
        let temperature = mainDict["temp"] as! NSNumber
        return temperature.stringValue
    }
    
    func getMinMaxTemperature() -> (String, String) {
        let mainDict = dictionaryToOperate["main"] as! Dictionary<String, Any>
        let maxTemp = mainDict["temp_max"] as! NSNumber
        let minTemp = mainDict["temp_min"] as! NSNumber
        return (maxTemp.stringValue, minTemp.stringValue)
    }
    
    func getMainWeatherDescription() -> String {
        let weatherDict = dictionaryToOperate["weather"] as! Array<Dictionary<String, Any>>
        let firstObject = weatherDict[0]
        return firstObject["main"] as! String
    }
    
    func getMinuteWeatherDetails() -> Dictionary<String, String>{
        var minuteDetailsDictionary = Dictionary<String, String>()
        
        let sysDict = dictionaryToOperate["sys"] as! Dictionary<String, Any>
        
        let riseMilliseconds: UnixTime = (sysDict["sunrise"] as! NSNumber).intValue
        let setMilliseconds: UnixTime = (sysDict["sunset"] as! NSNumber).intValue
        
        minuteDetailsDictionary["SUNRISE"] = String.init(format: "%@", riseMilliseconds.toHour)
        minuteDetailsDictionary["SUNSET"] = String.init(format: "%@", setMilliseconds.toHour)
        
        let mainDict = dictionaryToOperate["main"] as! Dictionary<String, Any>
        minuteDetailsDictionary["HUMIDITY"] = String.init(format: "%@%@", (mainDict["humidity"] as! NSNumber).stringValue, "%")
        minuteDetailsDictionary["PRESSURE"] = String.init(format: "%@ hPa", (mainDict["pressure"] as! NSNumber).stringValue)
        
        let windDict = dictionaryToOperate["wind"] as! Dictionary<String, Any>
        let windSpeed = windDict["speed"] as! NSNumber
        minuteDetailsDictionary["WIND"] = String.init(format: "%@ meter/sec", windSpeed.stringValue)
        
        let visibility = dictionaryToOperate["visibility"] as! NSNumber
        minuteDetailsDictionary["VISIBILITY"] = String.init(format: "%@ meter", visibility.stringValue)
        return minuteDetailsDictionary
    }
}
