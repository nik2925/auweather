//
//  AUWeather+ UnixTime.swift
//  AUWeather
//
//  Created on 03/11/17.
//

import Foundation
import UIKit

typealias UnixTime = Int

extension UnixTime {

    private func formatType(form: String) -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.init(identifier: "Australia/Melbourne")
        dateFormatter.dateFormat = form
        return dateFormatter
    }

    private func localFormatType(form: String) -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        dateFormatter.dateFormat = form
        return dateFormatter
    }
    
    var dateFull: Date {
        return Date(timeIntervalSince1970: Double(self))
    }
    
    var toHour: String {
        return formatType(form: "HH:mm a").string(from: dateFull)
    }
    
    var toLocalHour: String {
        return localFormatType(form: "HH:mm a").string(from: dateFull)
    }
    
    var toDay: String {
        return formatType(form: "MM/dd/yyyy").string(from: dateFull)
    }
}
