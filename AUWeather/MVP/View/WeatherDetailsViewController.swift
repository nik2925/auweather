//
//  WeatherDetailsViewController.swift
//  AUWeather
//
//  Created on 02/11/17.
//

import UIKit

class WeatherDetailsViewController: MVPBaseViewController {

    @IBOutlet weak var lblCityName: UILabel?
    @IBOutlet weak var lblWeatherDescription: UILabel?
    @IBOutlet weak var lblCurrentTemperature: UILabel?
    @IBOutlet weak var lblMaxTemperature: UILabel?
    @IBOutlet weak var lblMinTemperature: UILabel?
    @IBOutlet weak var tblDetails: UITableView?
    
    @IBOutlet weak var mainDetailsView: UIView?
    @IBOutlet weak var mainDetailsViewTopConstraint: NSLayoutConstraint?
    @IBOutlet weak var minuteDetailsView: UIView?
    @IBOutlet weak var minuteDetailsViewTopConstraint: NSLayoutConstraint?
    @IBOutlet weak var minuteDetailsViewHeightConstraint: NSLayoutConstraint?
    
    var weatherDetailsDictionary: Dictionary<String, Any>?
    
    var arrDetailsKeys: [String]?
    var detailValuesDict: Dictionary<String, String>?
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func createPresenter() -> MVPBasePresenter {
        return WeatherDetailsViewPresenter.init(view: self, weatherDetails: weatherDetailsDictionary!)
    }
    
    @IBAction func btnCloseClick() {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension WeatherDetailsViewController: WeatherDetailsView {
    
    func getPresenter() -> WeatherDetailsViewPresenter {
        return self.presenter as! WeatherDetailsViewPresenter
    }
    
    func configureView() {
        
        let dictionaryOperator = DictionaryOperations.init(withDictionary: weatherDetailsDictionary!)
        lblCityName?.text = dictionaryOperator.getCity()
        lblCurrentTemperature?.text = String(format:"%@%@", dictionaryOperator.getTemperature(), "\u{00B0}")
        lblWeatherDescription?.text = dictionaryOperator.getMainWeatherDescription()
        let temperatures = dictionaryOperator.getMinMaxTemperature()
        lblMaxTemperature?.text = String(format:"%@%@", temperatures.0, "\u{00B0}")
        lblMinTemperature?.text = String(format:"%@%@", temperatures.1, "\u{00B0}")
    }
    
    func displayDetails(_ details: [String], detailValues: Dictionary<String, String>) {
        arrDetailsKeys = details
        detailValuesDict = detailValues
        tblDetails?.reloadData()
    }
    
    func startAnimation() {
        
        mainDetailsView?.alpha = 0.0
        minuteDetailsViewHeightConstraint?.constant = 0.0
        UIView.animate(withDuration: 5.0, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 3.0, options: UIViewAnimationOptions.curveLinear, animations: { [weak self] in
            self?.mainDetailsView?.alpha = 1.0
            self?.mainDetailsViewTopConstraint?.constant = 50.0
            self?.view.layoutIfNeeded()
        }) { [weak self] (complete) in
            if complete == true {
                UIView.animate(withDuration: 1.0, animations: {
                    self?.minuteDetailsViewHeightConstraint?.constant = 300.0
                    self?.view.layoutIfNeeded()
                })
            }
        }
    }
    
}

extension WeatherDetailsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return (arrDetailsKeys?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherDetailsTableViewCell", for: indexPath) as! WeatherDetailsTableViewCell
        
        cell.lblDetail?.text = arrDetailsKeys?[indexPath.row]
        cell.lblDetailValue?.text = detailValuesDict?[arrDetailsKeys![indexPath.row]]
        
        if indexPath.row == (tableView.numberOfRows(inSection: 0) - 1) {
            cell.imgBottomBorder?.isHidden = true
        }
        return cell
    }
}

