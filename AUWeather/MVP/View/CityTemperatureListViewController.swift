//
//  CityTemperatureListViewController.swift
//  AUWeather
//
//  Created on 31/10/17.
//

import UIKit
import Toast_Swift
import AFNetworking

class CityTemperatureListViewController: UITableViewController {

    var activityIndicator: UIActivityIndicatorView?
    var cityWiseWeatherDetails = Dictionary<String, Any>()
    var cityIds = [String]()
    var selectedCity: String?
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        
        if !Utils.isInternetAvailable(){
            self.view.makeToast("Network not reachable!!", duration: 2.0, position: .center)
            return
        }
        addActivityindicator()
        let factory = CityWiseWeatherFactory.sharedFactory
        factory.configure(withDelegate: self)
        factory.getWeatherDetails()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cityIds.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityTemperatureTableViewCell", for: indexPath) as! CityTemperatureTableViewCell
        autoreleasepool{
            let weather = cityWiseWeatherDetails[cityIds[indexPath.row]] as! Dictionary<String, Any>
            let dictionaryOperator = DictionaryOperations.init(withDictionary: weather)
            cell.lblCityName?.text = dictionaryOperator.getCity()
            cell.lblTemperature?.text = String(format:"%@%@", dictionaryOperator.getTemperature(),"\u{00B0}")
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewsArray = Bundle.main.loadNibNamed("TableHeaderView", owner: self, options: nil)
        let headerView = viewsArray?[0] as! TableHeaderView
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCity = cityIds[indexPath.row]
        self.performSegue(withIdentifier: "showWeatherDetails", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let weatherDetailsViewController = segue.destination as! WeatherDetailsViewController
        weatherDetailsViewController.weatherDetailsDictionary = cityWiseWeatherDetails[selectedCity!] as? Dictionary<String, Any>
    }
}

extension CityTemperatureListViewController : CityWiseWeatherFactoryProtocol {
    func newWeatherDataRecorded(cityId: String, weatherDetails: Dictionary<String, Any>) {
        cityWiseWeatherDetails[cityId] = weatherDetails
        if !cityIds.contains(cityId) {
            cityIds.append(cityId)
        }
        if AUWeatherAPIClient.sharedClient.dataTasks.count == 0 {
            removeActivityIndicator()
        }
        self.tableView.reloadData()
    }
    
    func errorInGettingDetails(_ error: String) {
        if AUWeatherAPIClient.sharedClient.dataTasks.count == 0 {
            removeActivityIndicator()
            self.view.makeToast(error, duration: 2.0, position: .center)
        }
    }
}

//Activity indicator handling.
extension CityTemperatureListViewController {
    func addActivityindicator() {
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        activityIndicator?.center = view.center
        activityIndicator?.startAnimating()
        view.addSubview(activityIndicator!)
    }
    
    func removeActivityIndicator() {
        activityIndicator?.stopAnimating()
        activityIndicator?.removeFromSuperview()
    }
}
