//
//  MVPBasePresenter.swift
//  Messenger
//
//  Created on 08/08/17.
//  Copyright © 2017. All rights reserved.
//

import Foundation

class MVPBasePresenter {
    
    var viewInterface: Any?
    
    init<V: MVPBaseView>(_ view: V) {
        self.viewInterface = view
    }
    
    func onViewDidLoad(){
        
    }
    
    func onReceivingMemoryWarning(){
        
    }
    
    func beforeViewAppears() {
        
    }
    
    func beforeViewDisappear(){
        
    }
    
    func onViewAppearance(){
        
    }
    
    func onViewDisappearance(){
        
    }
}

protocol MVPBaseView {
    associatedtype Item
    func getPresenter() -> Item
}

protocol PresenterInterface {
    associatedtype Item
    func getView() -> Item
}
