//
//  MVPBaseViewController.swift
//  Messenger
//
//  Created on 08/08/17.
//  Copyright © 2017. All rights reserved.
//

import UIKit

enum ViewError: Error {
    case overrideCreatePresenter
    case overrideLeftbarButtonTapMethod
}

class MVPBaseViewController: UIViewController {

    var presenter: MVPBasePresenter?
    
    override func viewDidLoad() {
        try! self.presenter = self.createPresenter()
        self.presenter?.onViewDidLoad()
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        self.presenter?.onReceivingMemoryWarning()
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.presenter?.beforeViewAppears()
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.presenter?.onViewAppearance()
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.presenter?.beforeViewDisappear()
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.presenter?.onViewDisappearance()
        super.viewDidDisappear(animated)
    }
    
    func createPresenter() throws -> MVPBasePresenter {
        throw ViewError.overrideCreatePresenter
    }
}
