//
//  WeatherDetailsViewPresenter.swift
//  AUWeather
//
//  Created on 02/11/17.
//

import Foundation

class WeatherDetailsViewPresenter : MVPBasePresenter {
    
    var weatherDictionary: Dictionary<String, Any>?
    
    init<V: WeatherDetailsView>(view: V, weatherDetails: Dictionary<String, Any>) {
        self.weatherDictionary = weatherDetails
        super.init(view)
    }
    
    func getView() -> WeatherDetailsViewController {
        return self.viewInterface! as! WeatherDetailsViewController
    }
    
    override func onViewDidLoad() {
        self.getView().configureView()
        getMinuteWeatherDetails()
    }
    
    override func onViewAppearance() {
        self.getView().startAnimation()
    }
    
    func getMinuteWeatherDetails() {
        let arrWeatherDetailsKeys = ["SUNRISE", "SUNSET", "PRESSURE", "HUMIDITY", "WIND", "VISIBILITY"]
        let dictionaryOperator = DictionaryOperations.init(withDictionary: weatherDictionary!)
        self.getView().displayDetails(arrWeatherDetailsKeys, detailValues: dictionaryOperator.getMinuteWeatherDetails())
    }
}

protocol WeatherDetailsView: MVPBaseView {
    func configureView()
    func displayDetails(_ details: [String], detailValues: Dictionary<String, String>)
    func startAnimation()
}
