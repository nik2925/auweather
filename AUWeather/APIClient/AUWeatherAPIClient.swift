//
//  AUWeatherAPIClient.swift
//  AUWeather
//
//  Created on 31/10/17.
//

import Foundation
import AFNetworking

class AUWeatherAPIClient: AFHTTPSessionManager {
    
    static let sharedClient = AUWeatherAPIClient()
    
    convenience init() {
        let configuration = URLSessionConfiguration.default
        self.init(baseURL: AUWeatherBaseURLManager.baseURL(), sessionConfiguration: configuration)
    }
    
    override init(baseURL url: URL?, sessionConfiguration configuration: URLSessionConfiguration?) {
        super.init(baseURL: url, sessionConfiguration: configuration)
        self.responseSerializer = AFJSONResponseSerializer.init()
        
        let requestSerializer = AFJSONRequestSerializer.init()
        requestSerializer .setValue("application/json", forHTTPHeaderField: "Content-Type")
        requestSerializer .setValue("application/json", forHTTPHeaderField: "Accept")
        self.requestSerializer = requestSerializer
        self.operationQueue.maxConcurrentOperationCount = OperationQueue.defaultMaxConcurrentOperationCount
    }
    
    private func getAppId() -> String {
        return "3d2df02f659f0e5c46565a20ab934ce9"
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func getWeatherDetailsForCity(withId id: String, completionHandler:@escaping (Dictionary<String, Any>?, Error?) -> Swift.Void) {
        let url = String.init(format: "%@data/2.5/weather?id=%@&units=%@&appid=%@", self.baseURL!.absoluteString, id, WeatherMeasures.temperatureUnit, getAppId())
        let request = self.requestSerializer.request(withMethod: "GET", urlString: url, parameters: nil, error: nil)

        let task = self.dataTask(with: request as URLRequest) { (data, response, error) in
            completionHandler(response as? Dictionary<String, Any>, error)
        }
        task.resume()
    }
}
