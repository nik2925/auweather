//
//  AUWeatherBaseURLManager.swift
//  AUWeather
//
//  Created on 31/10/17.
//

import Foundation

class AUWeatherBaseURLManager {
    static func baseURL() -> URL {
        return URL.init(string: "http://api.openweathermap.org/")!
    }
}
