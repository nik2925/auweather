//
//  WeatherDetailsTableViewCell.swift
//  AUWeather
//
//  Created on 03/11/17.
//

import UIKit

class WeatherDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDetail: UILabel?
    @IBOutlet weak var lblDetailValue: UILabel?
    @IBOutlet weak var imgBottomBorder: UIImageView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
