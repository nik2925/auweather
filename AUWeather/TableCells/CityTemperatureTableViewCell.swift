//
//  CityTemperatureTableViewCell.swift
//  AUWeather
//
//  Created on 01/11/17.
//

import UIKit

class CityTemperatureTableViewCell: UITableViewCell {

    @IBOutlet weak var lblCityName: UILabel?
    @IBOutlet weak var lblTemperature: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
